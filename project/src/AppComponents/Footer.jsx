/**
 * @author Luke Weaver
 * Component for the page footer
 * @returns the footer element
 */
function Footer(){
    return (
        <footer>
            <h2>Project 2 - Using React</h2>
            <p>Daniel Giove, 1837062 / Pera Nicholls, 2145293 / Luke Weaver, 1937361 &copy; Fall 2022</p>
        </footer>
    );
}

export default Footer;