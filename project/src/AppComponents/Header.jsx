import SearchBar from '../SelectionComponents/SearchBar';

/**
 * @author Luke Weaver
 * Component for the page header, containing the search bar
 * @returns the header element
 */
function Header(){
    return (
        <header>
            <h2>Pseudo Forum</h2>
            <SearchBar/>
        </header>
    );
}

export default Header;