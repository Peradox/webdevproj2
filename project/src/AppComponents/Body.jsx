import ColumnStart from "../ColumnComponents/ColumnStart";
import ColumnMain from "../ColumnComponents/ColumnMain";
import ColumnEnd from "../ColumnComponents/ColumnEnd";

/**
 * @author Luke Weaver
 * Component of the body, containing the 3 main columns
 * @returns the body div
 */
function Body(){
    return (
        <div id='bodyDiv'>
            <div id='columnDiv'>
                <ColumnStart/>
                <ColumnMain/>
                <ColumnEnd/>
            </div>
        </div>
        
    );
}

export default Body;