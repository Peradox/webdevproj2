/**
 * This takes as input an array of objects and strings representing object keys to make a table. 
 * @author Pera Nicholls
 * 
 * @param {Object} props contains:
 *                 maxRows: maximum number of rows
 *                 Array array: holds objects that will be represented in the table
 *                 Array fields: strings for object keys to be shown
 * @returns {HTMLDivElement}
 */
function Table(props){  

    let rowArr=[];
    let columnTitlesArr=props.fields.map(field=><th className= "th1">{field}</th>);
    rowArr.push(<tr><td className="td1" colSpan={props.array.length}>{props.header}</td></tr>);
    rowArr.push(<tr>{columnTitlesArr}</tr>);
    for (let i=0;i<props.maxRows;i++){
        let cellArr=[];
        
        /*for(let i=0;i<props.columns;i++){
            let newCell=<td>placeholder</td>;
            cellArr.push(newCell);
        }
        */
        for(let j=0;j<props.fields.length;j++){
            let newCell=
            (
                <td className="td2">{props.array[i][props.fields[j]]}</td>
            );
                cellArr.push(newCell);
        }
        let newRow=<tr>{cellArr}</tr>;
        rowArr.push(newRow);
    }
    return (
    <div className="tableBackground">
        <table>{rowArr}</table>
        </div>
        );
}

           

export default Table;