import './css/App.css';
import Body from './AppComponents/Body.jsx';
import Header from './AppComponents/Header';
import Footer from './AppComponents/Footer';
//import fetchData from './json/fetchData';

//console.log(fetchData);

function App() {
  return (
    <div>
      
      <Header/>
      <Body/>
      <Footer/>
    </div>
  );
}

export default App;