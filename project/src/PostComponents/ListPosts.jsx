import fetchData from '../json/fetchData';
import * as React from "react";

let data = fetchData();

/**
 * @author Luke Weaver, Pera Nicholls
 * Component for ListPosts, which contains all posts that you want to display,
 * depending on the chosen settings
 * @param {*} props contains:
 *    posts: which has an array of post Objects to display
 *    refresh(): which is a function that updates the posts state in ColumnMain
 * @returns the div containing desired posts
 */
function ListPosts(props){
    function deletePost(dTopic_id){
        if(window.confirm("Are you sure you want to delete this post? This cannot be undone!")){
            data.posts = data.posts.filter(post => post.topic_id !== dTopic_id);

            props.update(selectedPosts(props.posts));
        }
    }

    let i = 0;
    return (
        <div id="listPosts">
            {
                props.posts.map((post) => {
                    return(
                        <div key={i} id={i++} className="post" onClick={likeDislike}>
                            <div className="postTop">
                                <p className="postTitle">{post.text}</p>
                                <p className="likes">likes: {post.like}</p>
                                <button className="likeBtn"> <img src="/like.png" className="likeBtn"/> </button>
                                <button className="dislikeBtn"> <img src="/dislike.png" className="dislikeBtn"/></button>
                            </div>
                            <div className="postBottom">
                                <p>author: {post.author}</p>
                                <p>date: {post.date}</p>
                                <p>replies: {post.replies}</p>
                                <button onClick={() => deletePost(post.topic_id)}>trash</button>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    );   
    
}

/**
 * @author Luke Weaver
 * A function that gets the desired category and topic
 * and gets all posts that use them
 * @returns posts, an array of post Objects
 */
export function selectedPosts(){
    const catDD = document.querySelector('#categoryDD');
    const topicDD = document.querySelector('#topicDD');
    let posts = [];
    let selectedCategory;
    let selectedTopic;

    if(catDD !== null && topicDD !== null){
        selectedCategory = catDD.selectedIndex + 1;
        selectedTopic = topicDD.selectedIndex + 1 || 1;
    }else{
        selectedCategory = 1;
        selectedTopic = 1;
    }

    data.posts.forEach(
        (post) => {
            if(post.topic_id.includes(`cat${selectedCategory}_topic${selectedTopic}`)){
                posts.push(post);
            }
        }
    );

    return posts;
}

/**
 * This increments/decrements the likes counter when the respective button is clicked
 * @author Pera Nicholls
 * 
 * @param {Event} e 
 */
function likeDislike(e){
    let l=0;
    Array.from(document.getElementsByClassName("likes")).forEach(
        (textarea)=>{
            if(e.currentTarget.contains(textarea)){
                let numberLikes=Number(textarea.innerText.replace(/[^0-9]/g,''));
                l = numberLikes;

                if(e.target&&e.target.className==="likeBtn"){
                    l++;
                }
                else if (e.target&&e.target.className==="dislikeBtn" && numberLikes!==0){
                    l--;
                }
                textarea.innerText=`likes: ${l}`;
            }
        }
    );
}

export default ListPosts;