import jsonData from './forum-data.json';
import userData from './users-data.json';
//let jsonData=getJSON('https://sonic.dawsoncollege.qc.ca/~nasro/js320/project2/forum.json');
//let userData=getJSON('https://sonic.dawsoncollege.qc.ca/~nasro/js320/project2/users.json');

/**
 * This uses fetch to get and parse JSON data (did not work with the URLs, was giving me security errors)
 * @author Pera Nicholls
 * 
 * @param {String} url 
 * @returns Object
 */
function getJSON(url){
    let jsonObj;
    fetch(url)
    .then(response=>{
    if(response.ok){
        return response.json();
    }
    else{
        throw Error(response.statusText);
    }
    })
    .then(responseObj=>jsonObj=responseObj)
    .catch(error=>{console.log(error)});
    return jsonObj;
}

/**
 * @author Luke Weaver
 * A function that gets all useful data from the JSON data
 * @returns fetchData, an object containing all the data
 */
const fetchData = function createFetchData(){
    const posts = [];
    const categories = [];
    const topics = [];
    const users = [];

    //get all categories, topics, and posts
    jsonData.categories.forEach(
        (category) =>{
            categories.push(category);
            category.topicList.forEach(
                (topic) => {
                    topics.push(topic);
                    topic.listPosts.forEach(
                        (post) => {
                            posts.push(post);
                        }
                    )
                }
            );
        }
    );

    userData.users.forEach(
        (user) => {
            users.push(user);
        }
    )

    const fetchData = {
        posts:posts,
        categories:categories,
        topics:topics,
        users:users
    }

    return fetchData;
}

export default fetchData;