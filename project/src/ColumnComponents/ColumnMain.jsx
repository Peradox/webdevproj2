import ListPosts from "../PostComponents/ListPosts";
import TopSettings from "../SelectionComponents/TopSettings";
import { selectedPosts } from "../PostComponents/ListPosts";
import * as React from "react";

/**
 * @author Luke Weaver
 * Component for the middle column, containing the TopSettings and ListPosts components
 * @returns the middle column div
 */
function ColumnMain(){
    const [posts, setPosts] = React.useState(selectedPosts);

    const update = (input) => {
        if(input){
            setPosts(input);
        }else{
            setPosts(selectedPosts);
        }
    }

    return (
        <div className="column">
            <TopSettings handleChange={update}/>
            <ListPosts update={update} posts={posts}/>
        </div>
    );
}

export default ColumnMain;