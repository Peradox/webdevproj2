import Table from "../TableComponents/Table";
import fetchData from "../json/fetchData";

let data=fetchData();

/**
 * @author Luke Weaver, Pera Nicholls
 * Component for the right column, containing the data tables
 * @returns the right column div
 */
function ColumnEnd(){
    return (
        <div className="column">
            <Table header={"Topic Stats"} array={data.topics.sort((a,b)=>b.nberPost-a.nberPost)} maxRows={data.topics.length} fields={["topic_title","nberPost","status"]} />
            <Table header={"Recent Posts"} array={data.posts.sort(recentPostsCallback)} maxRows={4} fields={["author","date","rate"]} />
            <Table header={"User stats"} array={data.users.sort((a,b)=>b.nberPosts-a.nberPosts)} maxRows={data.users.length} fields={["user_id", "nberPosts"]}/>
        </div>
    );
}

/**
 * @author Pera Nicholls
 * 
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function recentPostsCallback(a,b){
    let first=a.date.split(/-| |:/); //split on dash or space or colon
    let second=b.date.split(/-| |:/);

    for(let i=0;i<first.length;i++){
        if(first[i]!==second[i]){
            return parseInt(second[i]-first[i]); //goes through each numeric time value to sort by most recent
        }
    }

}

export default ColumnEnd;