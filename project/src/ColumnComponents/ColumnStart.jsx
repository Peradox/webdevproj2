/**
 * @author Luke Weaver
 * Component for the left column, containing the dummy admin panel
 * @returns the left column div
 */
function ColumnStart(){
    return (
    <div id="leftColumn" className="column">
        <p> Dummy Components (no functionality)</p>
 
            <div id='adminDiv'>
                <p id="admin">A D M I N</p>
                <button class="b1">Create Category</button>
                <button class="b1">Create Topic</button>
                <button class="b1">Close Topic</button>
                <button class="b1">Delete Topic</button>
            </div>
    </div>
    );
}

export default ColumnStart;