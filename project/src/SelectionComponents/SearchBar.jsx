import fetchData from '../json/fetchData';
import {useState} from "react";

let data = fetchData();


/**
 * @author Daniel Giove
 * 
 * @returns {HTMLInputElement}
 */
function SearchBar(){
    const [userInput, setUserInput] = useState("");
        if(userInput!==""){
            //this is unused, but prints an array of post objects based on the user's input
            console.log(data.posts.filter((post)=>
            post.text.toLowerCase().includes(userInput.toLowerCase()) ||
            post.author.toLowerCase().includes(userInput.toLowerCase())));            
        }
        else{
            console.log("oops!");
        }
    

    return (
        
        <input type='text'  placeholder='Search Posts...' id='searchBar' onBlur={(e)=>setUserInput(e.target.value)}></input>
        
        
    );
}

export default SearchBar;