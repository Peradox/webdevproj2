import DropDown from "../DropDownComponents/DropDown";

/**
 * @author Luke Weaver
 * Component for the TopSettings div,
 * containing the Dropdown menus for selecting the category and topic
 * @param {*} props contains the function to update the posts state in ColumnMain
 * @returns the TopSettings div
 */
function TopSettings(props){
    const updateState = props.handleChange;

    return (
        <div id="topSettings">
            <DropDown handleChange={updateState}/>
        </div>
    );
}

export default TopSettings;