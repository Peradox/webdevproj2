import * as React from "react";
import fetchData from '../json/fetchData';

let data = fetchData();

/**
 * @author Luke Weaver
 * Component for the Dropdown menus
 * @param {*} props 
 * @returns 
 */
function DropDowns (props){
    let nameOptions = [];
    let topicOptions = [];

    const [selectedOption, setSelected] = React.useState(1);
    const [childOptions, setTopicOptions] = React.useState([]);

    /**
     * A function that is called when the category select has been changed
     * It updates the state of the selected option and the topic options,
     * then calls the function that updates the list of posts
     */
    const handleCatChange = () => {
        //fixes topic being stuck at 2 when changed to a category that only has one topic
        let categoryDD = document.querySelector(`#categoryDD`);
        let topicDD = document.querySelector(`#topicDD`);
        topicDD.selectedIndex = 0;

        //+1 because ids start at 1 in JSON
        setSelected(categoryDD.selectedIndex);
        
        topicOptions = updateTopic(categoryDD.selectedIndex);
        setTopicOptions(topicOptions);

        props.handleChange();
    };

    /**
     * A function that is called when the topic select has been changed
     * It calls the function that updates the list of posts
     */
    const handleTopicChange = () => {
        props.handleChange();
    };

    //initialize name list
    data.categories.forEach(
        (category) => {
            nameOptions.push(category.name);
        }
    );

    //initialize topic list with category 1 topics
    data.topics.forEach(
        (topic) => {
            topicOptions.push(topic.topic_title);
        }
    );

    return (
        <>
        
        <h3> Choose Category 
            <select onChange={handleCatChange} id='categoryDD' className='dropdowns'>
                {createSelectOptions(nameOptions)}
            </select>
            </h3>
            <h3> Choose Topic 
            <select onChange={handleTopicChange} id='topicDD' className='dropdowns'>
                {
                    childOptions.map((option) => (
                        <option key={option} value={option}>
                            {option}
                        </option>
                    ))
                }
            </select>
            </h3>
        </>
    );
}

/**
 * @author Luke Weaver
 * A function that creates an array of all options JSX Elements, given an array of topic titles
 * @param {String[]} items an array of topic titles
 * @returns 
 */
function createSelectOptions(items){
    let options = [];

    for (let i = 0; i < items.length; i++){
        options.push(<option key={i} value={items[i]}>{items[i]}</option>);
    }

    return options;
}

/**
 * @author Luke Weaver
 * A function that gets all topics related to the selected category
 * @param {Number} selectedOption the index of the selected category
 * @returns topicOptions, an array of relevant topics
 */
function updateTopic(selectedOption){
    let topicOptions = [];
    
    for(let i = 0; i < data.categories[selectedOption].topicList.length; i++){
        topicOptions.push(data.categories[selectedOption].topicList[i].topic_title);
    }

    return topicOptions;
}

export default DropDowns;