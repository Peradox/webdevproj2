# Web Development Project 2

# Project Description

The web application allows the user to browse a pseudo-forum. Users can navigate the posts by selecting desired Categories and Category-related Topics using the dropdown menus, or the search bar. They can also like, dislike, and delete posts. Stats about the topics, posts, and users are displayed in tables on the side.

# Setup

Step 1: Open the 'WebDevProj2' folder in a Workspace in VSCode.
Step 2: Create a new terminal.
Step 3: Change to the 'project' directory by entering 'cd project'.
Step 4: Install node_modules by entering 'npm install'.
Step 5: Run the app by entering 'npm run start'.